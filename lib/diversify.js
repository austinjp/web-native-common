import { React, h } from "./react-preact"
import { platform } from "./platform"
import { styler } from "./styler"

// FIXME See https://reactnative.dev/docs/intro-react-native-components#core-components

// The iterative approach here was based on react-traverse
// which I couldn't get working, and https://stackoverflow.com/a/32967919
// and https://stackoverflow.com/q/52567799

function diversify(o) {
  const alreadyDone = new Map()
  return traverse(o,alreadyDone)
}

function autoswitch(o) {

  var props = (o || {}).props || {}
  var type =  (o || {}).type  || {}
  var element = ( type.displayName || type.name || "View" )

  // console.log("props:",props)
  // console.log("type:",type)
  // console.log("element:",element)

  let E;

  if (platform.web) {

    if (props.style) {
      props.className = styler(props.style)
      delete(props.style)
    }

    switch(element) {
      case "TextInput":
        E = "Input"
        break;
      case "ScrollView":
        E = "div"
        break;
      case "Image":
        E = "img"
        break;
      case "Text":
        E = "span"
        break;
      default:
        E = "div"
        break;
    }
    console.log("Changing " + element + " to " + E)

  } else if (platform.device) {

    const RN = require("react-native")

    let _instance;
    try {
      _instance = <o {...props} />
    } catch {}
    if (_instance) {
      return traverse(_instance,alreadyDone)
    }

    switch(element) {
      case "TextInput":
        E = RN.TextInput
        break;
      case "ScrollView":
        E = RN.ScrollView
        break;
      case "Image":
        E = RN.Image
        break;
      case "Text":
        E = RN.Text
        break;
      default:
        E = RN.View
        break;
    }

  }

  // Note: this works for both React and React Native:
  return React.createElement(E,props)
}

const traverse = (node,alreadyDone) => {
  return _traverse(node,alreadyDone)
}

const _traverse = (node,alreadyDone) => {
  // console.log(" =====>> ",node)
  if (node.props && node.props.children) {
    const children = React.Children.toArray(node.props.children);
    children.forEach((child) => {
      if (! alreadyDone.has(node)) {
        alreadyDone.set(node,_traverse(child,alreadyDone))
      }
      return alreadyDone.get(node)
    })
  }
  return autoswitch(node)
}

export default diversify
export { diversify }
