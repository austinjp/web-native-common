import { React, h, Component, render } from "../../../lib/react-preact"
import { diversify } from "../../../lib/diversify"

class View extends Component {
  constructor(props) {
    super(props)
  }

  render(props) {
    // const styles = (this.props || {}).style || {}
    // return diversify({element:"view",props:this.props,styles:styles})
    return ( <div>{this.props.children}</div> )
  }
}

export default View
export { View }
