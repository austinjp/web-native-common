import { React, h, Component, render } from "../../../lib/react-preact"
import { diversify } from "../../../lib/diversify"
import { Text as _Text } from "react-native"

class Text extends Component {
  constructor(props) {
    super(props)
  }

  render(props) {
    return diversify(
      <div {...this.props}>{this.props.children}</div>
    )
  }
}

export default Text
export { Text }
