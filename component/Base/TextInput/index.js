import { React, h, Component, render, Fragment } from "../../../lib/react-preact"
import { diversify } from "../../../lib/diversify"
import { platform } from "../../../lib/platform"

class TextInput extends Component {
  constructor(props) {
    super(props)
  }

  render(props) {
    let p = this.props
    if (platform.web) {
      p.type = "text"
    }
    return diversify({element:"textinput",props:p})
  }
}

export default TextInput
export { TextInput }
