import { React, h, Component, render, Fragment } from "../../../lib/react-preact"
import { diversify } from "../../../lib/diversify"
import { platform } from "../../../lib/platform"

class Image extends Component {
  constructor(props) {
    super(props)
  }

  render(props) {
    let p = this.props
    if (platform.web) {

      // RN uses source.uri instead of src for image URIs.
      p.src = p.src ? p.src : ((p || {}).source || {}).uri
      delete(p.source)

      // RN does not specify units, which are pixels.
      if ("style" in p) {
        if ("height" in p.style) {
          p.style.height = parseInt(p.style.height) + "px" }
        if ("width" in p.style) {
          p.style.width = parseInt(p.style.width) + "px" }
      }
    }
    return diversify({element:"image",props:p})
  }
}

export default Image
export { Image }
