import { React, h, Component, Fragment, render } from "../../lib/react-preact"
import { Image, Text, TextInput, View } from "../Base"
import diversify from "../../lib/diversify"

class Test extends Component {
  constructor(props) {
    super(props)
  }

  render(props) {
    return diversify(
      <>
        <View style={{flex:1,justifyContent:"center",alignItems:"stretch"}}>
          <Text style={{color:"#f00"}}>Woot?</Text>
        </View>
        {this.props.children}
      </>
    )
  }
}

export default Test
export { Test }
